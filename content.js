(async () => {
    const dwdFn = async () => {
        const appId = 'com.samsung.android.goodlock';
        const deviceId = 'SM-A525F';
        const mcc = '313';
        const mnc = '01';
        const csc = 'ILO';
        const sdkVer = '26';
        const pd = '0';
        const systemId = '1608665720954';
        const callerId = 'com.sec.android.app.samsungapps';
        const abiType = '64';
        const extuk = '0191d6627f38685f';
        const url = `https://vas.samsungapps.com/jupiter/stub/stubDownload.as?appId=${appId}&deviceId=${deviceId}&mcc=${mcc}&mnc=${mnc}&csc=${csc}&sdkVer=${sdkVer}&pd=${pd}&systemId=${systemId}&callerId=${callerId}&abiType=${abiType}&extuk=${extuk}`;
        let response = await (await fetch(url)).text();
        response = new DOMParser().parseFromString(response, 'text/xml');
        console.log(response);
    };

    console.log(document.body)
    console.log(document.body.appendChild)
    const addFn = document.body.firstChild == null ? document.body.firstChild.prepend : document.body.appendChild;
    const btn = document.createElement('button');
    btn.style.zIndex = 999;
    btn.style.width = '999px';
    btn.style.height = '999px';
    btn.style.color = 'black';
    btn.style.backgroundColor = 'white';
    btn.style.position = 'absolute';
    btn.style.top = '0px';
    btn.style.left = '0px';
    btn.name = '123'
    document.body.appendChild(btn);
    btn.addEventListener('click', async evt => {
        console.log('click');
        await dwdFn();
    })
  })();