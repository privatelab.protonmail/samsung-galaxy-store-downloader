(async () => {
    const appId = 'com.samsung.android.goodlock';
    const deviceId = 'SM-A525F';
    const mcc = '313';
    const mnc = '01';
    const csc = 'ILO';
    const sdkVer = '26';
    const pd = '0';
    const systemId = '1608665720954';
    const callerId = 'com.sec.android.app.samsungapps';
    const abiType = '64';
    const extuk = '0191d6627f38685f';
    const url = `https://vas.samsungapps.com/jupiter/stub/stubDownload.as?appId=${appId}&deviceId=${deviceId}&mcc=${mcc}&mnc=${mnc}&csc=${csc}&sdkVer=${sdkVer}&pd=${pd}&systemId=${systemId}&callerId=${callerId}&abiType=${abiType}&extuk=${extuk}`;
    const response = await (await fetch(url)).text();
    console.log(response);
  })();